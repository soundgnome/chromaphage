extends Node2D

export var color = 'red'
var colors
var group = 'bosses'

func _ready():
	if self.color == 'blue':
		self.hide()
		$Area2D/Sprite.region_rect.position.x = 101
		self.colors = [GLOBALS.COLOR.RED, GLOBALS.COLOR.GREEN, GLOBALS.COLOR.BLUE]
	elif self.color == 'green':
		self.hide()
		$Area2D/Sprite.region_rect.position.x = 51
		self.colors = [GLOBALS.COLOR.RED, GLOBALS.COLOR.GREEN]
	else:
		self.colors = [GLOBALS.COLOR.RED]

	add_to_group(self.group)
	var echo = self._clone_node($Area2D)
	echo.transform.origin.x += GLOBALS.WORLD_WIDTH_PIXELS
	self.add_child(echo)


func _on_Area2D_body_entered(body):
	self.get_owner().set_colors(self.colors)
	var bosses = self.get_tree().get_nodes_in_group(self.group)
	if len(bosses) > 1:
		bosses[1].show()
	self.queue_free()


func _clone_node(orig):
	var clone = orig.duplicate()
	for child in orig.get_children():
		clone.add_child(self._clone_node(child))
	return clone
