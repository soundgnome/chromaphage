extends KinematicBody2D

const FLOOR_NORMAL = Vector2(0, -1)
const MOVEMENT_SCALE = 50

var WALK_SPEED = 5
var JUMP_SPEED = 20
var wrapped = false
onready var GRAVITY = ProjectSettings.get_setting('physics/2d/default_gravity')
onready var animator = $AnimationPlayer
onready var camera = $Camera2D
onready var drag_margins = [$Camera2D.drag_margin_left, $Camera2D.drag_margin_right]
onready var sprite = $Sprite

var velocity = Vector2(0,0)

func _ready():
	WALK_SPEED *= MOVEMENT_SCALE
	JUMP_SPEED *= MOVEMENT_SCALE
	GRAVITY *= MOVEMENT_SCALE

func _physics_process(delta):

	self._wrap()

	self.velocity.y += delta * GRAVITY
	self.velocity = self.move_and_slide(self.velocity, FLOOR_NORMAL)

	if self.is_on_floor():

		if Input.is_action_just_pressed('jump'):
			self.velocity.y -= JUMP_SPEED
		else:
			self.velocity.y = 0

		if Input.is_action_pressed('move_left'):
			self._walk('left')
		elif Input.is_action_pressed('move_right'):
			self._walk('right')
		else:
			self.velocity.x = 0
			self.animator.stop()

	elif self.velocity.x == 0:

		if Input.is_action_pressed('move_left'):
			self._walk('left')
		elif Input.is_action_pressed('move_right'):
			self._walk('right')


func _walk(direction):
	var factor = -1 if direction == 'left' else 1
	self.velocity.x = factor * WALK_SPEED
	if not self.animator.is_playing():
		self.sprite.scale.x = factor
		self.animator.play('walk')


func _wrap():
	if self.position.x < GLOBALS.WRAP_BOUNDARIES[0]:
		self.camera.drag_margin_right = -1
		self.position.x += GLOBALS.WORLD_WIDTH_PIXELS
		self.wrapped = true
	elif self.position.x > GLOBALS.WRAP_BOUNDARIES[1]:
		self.camera.drag_margin_left = -1
		self.position.x -= GLOBALS.WORLD_WIDTH_PIXELS
		self.wrapped = true
	elif self.wrapped:
		self.camera.drag_margin_left = self.drag_margins[0]
		self.camera.drag_margin_right = self.drag_margins[1]
		self.wrapped = false
