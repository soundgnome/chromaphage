extends TileMap

var world
onready var children = {
	GLOBALS.COLOR.RED: $Red,
	GLOBALS.COLOR.GREEN: $Green,
	GLOBALS.COLOR.BLUE: $Blue,
}

func _ready():
	self.world = self.get_owner()
	for i in self.children:
		self.children[i].hide()

func set_colors(colors):
	var color_mask = 1
	var collision_mask = 8
	for color in colors:
		var child = self.children[color]
		for pos in child.get_used_cells():
			var tile = max(self.get_cellv(pos), 0) | color_mask
			self.set_cellv(pos, tile)
		color_mask = color_mask << 1

	for pos in self.get_used_cells():
		var above = Vector2(pos.x, pos.y-1)
		var collision_tile = self.get_cellv(pos) | collision_mask
		if (self.get_cellv(above) | collision_mask) != collision_tile:
			self.set_cellv(pos, collision_tile)

	self.world.duplicate_tiles(self)
