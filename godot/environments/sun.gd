extends Sprite

const DIAMETER = 50
onready var COLOR = GLOBALS.COLOR.YELLOW

func set_colors(colors):
	var mask = 0
	for color in colors:
		mask = mask | color
	mask = mask & self.COLOR
	self.region_rect = Rect2(Vector2(mask * self.DIAMETER, 0), self.region_rect.size)
