extends Node2D

var platforms
var sun

func _ready():
	self.platforms = self.find_node('Platforms')
	self.sun = self.find_node('Sun')
	self.duplicate_tiles(self.find_node('Ground'))


func duplicate_tiles(tilemap):
	for pos in tilemap.get_used_cells():
		tilemap.set_cell(
			pos.x + GLOBALS.WORLD_WIDTH_TILES,
			pos.y,
			tilemap.get_cellv(pos)
		)


func set_colors(colors):
	var spec = {
		GLOBALS.COLOR.RED: '00',
		GLOBALS.COLOR.GREEN: '00',
		GLOBALS.COLOR.BLUE: '00',
	}
	for color in colors:
		spec[color] = 'ff'
	var sky_color = '00' + spec[GLOBALS.COLOR.GREEN] + spec[GLOBALS.COLOR.BLUE]
	ProjectSettings.set_setting('rendering/environment/default_clear_color', sky_color)
	self.platforms.set_colors(colors)
	self.sun.set_colors(colors)
