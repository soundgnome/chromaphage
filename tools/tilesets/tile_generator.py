class TileGenerator:

    def __init__(self, tile_width=48, padding=1):

        self._tile_width = tile_width
        self._half_tile = tile_width / 2
        self._padding = 1
        self._padded_width = tile_width + (2*padding)
        self._colors = [
            'grey',
            'red',
            'green',
            'yellow',
            'blue',
            'magenta',
            'cyan',
            'white',
        ]


    def get_all_tiles(self):
        tiles = []
        for collision in (False, True):
            name_pattern = '%s_Collision'if collision else '%s'
            i = 0
            for color in self._colors:
                tiles.append({
                    'name': name_pattern % color.capitalize(),
                    'offset': self._half_tile,
                    'region': [
                        (self._padded_width * i) + self._padding,
                        self._padding,
                        self._tile_width,
                        self._tile_width,
                    ],
                    'collision': collision,
                    'one_way_collision': bool(i != 0),
                })
                i += 1
        return tiles


    def get_tile(self, color, collision=False, one_way_collision=True):
        return {
            'name': color.capitalize(),
            'offset': self._half_tile,
            'region': [
                (self._padded_width * self._colors.index(color)) + self._padding,
                self._padding,
                self._tile_width,
                self._tile_width,
            ],
            'collision': collision,
            'one_way_collision': one_way_collision,
        }
