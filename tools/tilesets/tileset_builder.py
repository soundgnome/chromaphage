#!/usr/bin/env python3

from sys import stdout
from tile_generator import TileGenerator

class TilesetBuilder:

    def build(self, color, stream):
        stream.write(self._build_header())
        stream.write(self._build_tile_definition(color))


    def _build_header(self, resource='platforms.svg'):
        sections = [
            '[gd_resource type="TileSet" load_steps=3 format=2]',
            '[ext_resource path="res://assets/%s" type="Texture" id=1]',
            '[sub_resource type="RectangleShape2D" id=1]',
            'custom_solver_bias = 0.0\nextents = Vector2( 24, 24 )',
            '',
        ]
        return '\n\n'.join(sections) % resource


    def _build_tile_definition(self, color):
        generator = TileGenerator()
        lines = ['[resource]', '']
        i = 0
        if color == 'all':
            tiles = generator.get_all_tiles()
        elif color == 'grey':
            tiles = [ generator.get_tile('grey', collision=True, one_way_collision=False) ]
        else:
            tiles = [ generator.get_tile(color) ]
        for tile in tiles:
            lines += self._generate_tile_lines(i, tile)
            i += 1
        return '\n'.join(lines) + '\n'


    def _generate_tile_lines(self, i, tile):
        lines = [
            '%d/name = "%s"' % (i, tile['name']),
            '%d/texture = ExtResource( 1 )' % i,
            '%d/tex_offset = Vector2( 0, 0 )' % i,
            '%d/modulate = Color( 1, 1, 1, 1 )' % i,
            '%d/region = Rect2( %s )' \
            % (i, ', '.join(str(j) for j in tile['region'])),
            '%d/is_autotile = false' % i,
            '%d/occluder_offset = Vector2( %d, %d )' \
            % (i, tile['offset'], tile['offset']),
            '%d/navigation_offset = Vector2( %d, %d )' \
            % (i, tile['offset'], tile['offset']),
        ];

        if tile['collision']:
            lines += [
                '%d/shapes = [ {' % i,
                '"autotile_coord": Vector2( 0, 0 ),',
                '"one_way": %s,' \
                % ('true' if tile['one_way_collision'] else 'false'),
                '"shape": SubResource( 1 ),',
                '"shape_transform": Transform2D( 1, 0, 0, 1, %d, %d )' \
                % (tile['offset'], tile['offset']),
                '} ]',
            ]
        else:
            lines.append('%d/shapes = [  ]' % i)

        return lines




if __name__ == '__main__':
    colors = (
        'grey',
        'red',
        'green',
        'blue',
        'all',
    )
    builder = TilesetBuilder()
    for color in colors:
        with open('../../godot/tilesets/%s.tres' % color, 'w') as handle:
            builder.build(color, handle)
